package handler

import (
	"fmt"
	"net/http"

	"localisation-cache/internal/message"
	"localisation-cache/lib/cache"
)

type localisationHandler struct{}

func (localisationHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	keyword := r.URL.Path

	// Look up from cache
	fmt.Printf("Looking %s from cache... ", keyword)
	value, err := cache.Get(keyword)
	if err == nil {
		fmt.Printf("success! Returning ( %s ) as response\n", value)
		fmt.Fprint(w, value)
		return
	}

	// Look up failed, trying other method to resolve?
	fmt.Printf("failed. Attempting to resolve... ")

	// Resolve by reload string
	// message.Populate()
	// value, err = cache.Get(keyword)
	// if err == nil {
	// 	fmt.Printf("success! Returning ( %s ) as response\n", value)
	// 	fmt.Fprint(w, value)
	// 	return

	fmt.Printf("KEY ( %s ) NOT FOUND\n", keyword)
	fmt.Fprint(w, "ERROR: key not found")
}

func NewLocalisationHandler() *localisationHandler {
	message.Populate()
	return &localisationHandler{}
}
