package message

import (
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"localisation-cache/lib/cache"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
)

type messageJson struct {
	KeyName  string `json:"keyName"`
	Language string `json:"language"`
	Value    string `json:"value"`
}

type messageListJson struct {
	Messages []messageJson `json:"results"`
}

var (
	APIEndpoint  = os.Getenv("MESSAGE_ENDPOINT")
	LanguageCode = os.Getenv("LANGUAGE_CODE")
)

func validateYMLPair(key string, val string) error {
	if strings.Contains(val, "\"") {
		return errors.New("YAML pair value contains \" symbol")
	}
	return nil
}

func Populate() {
	customClient := http.Client{
		Timeout: time.Second * 5,
	}

	// Uncomment when v1 API is pushed
	// payload := []byte(`{"appName":"1","language":"4"}`)
	// req, err := http.NewRequest(http.MethodPost, APIEndpoint, bytes.NewBuffer(payload))
	req, err := http.NewRequest(http.MethodGet, APIEndpoint, nil)
	if err != nil {
		log.Fatal(err)
	}

	res, getErr := customClient.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}

	bodyExpended := `{"results":` + string(body) + `}`
	bodyExpendedByte := []byte(bodyExpended)

	newMessages := messageListJson{}
	jsonErr := json.Unmarshal(bodyExpendedByte, &newMessages)
	if jsonErr != nil {
		log.Fatal(jsonErr)
	}

	// Valid File
	validFile, fileErr := os.Create("./i18n/" + LanguageCode + ".yml")
	if fileErr != nil {
		panic(fileErr)
	}
	defer validFile.Close()

	validWriter := bufio.NewWriter(validFile)

	// Error File
	errorFile, fileErr := os.Create("./temp/" + LanguageCode + ".notyml")
	if fileErr != nil {
		panic(fileErr)
	}
	defer errorFile.Close()

	errorWriter := bufio.NewWriter(errorFile)

	for _, message := range newMessages.Messages {
		key := message.KeyName
		val := message.Value
		keyExpanded := key + "-" + message.Language

		validErr := validateYMLPair(key, val)
		switch validErr {
		case nil:
			cache.Set(keyExpanded, val)
			_, err := validWriter.WriteString(fmt.Sprintf("%s: \"%s\"\n", key, val))
			if err != nil {
				log.Fatal(err)
			}
		default:
			_, err = errorWriter.WriteString(fmt.Sprintf("%s: \"%s\"\n", key, val))
			if err != nil {
				log.Fatal(err)
			}
		}
	}

	validWriter.Flush()
	errorWriter.Flush()
}
