package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
)

type downloadHandler struct{}

func (downloadHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	tmpl, err := template.ParseFiles("./static/download.html")
	if err != nil {
		fmt.Println("template not found")
		return
	}
	tmpl.Execute(w, nil)
}

func main() {
	http.Handle("/i18n/", http.StripPrefix("/i18n/", http.FileServer(http.Dir("./i18n"))))
	http.Handle("/file/", http.StripPrefix("/file", downloadHandler{}))

	fmt.Println("Cacher is serving on port 3005...")
	log.Fatal(http.ListenAndServe(":3005", nil))
}
