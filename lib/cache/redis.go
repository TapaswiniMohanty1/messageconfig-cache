package cache

import (
	"github.com/gomodule/redigo/redis"
)

var (
	connectionPool redis.Pool
	connection     redis.Conn
)

func init() {
	connectionPool = redis.Pool{
		MaxIdle:   80,
		MaxActive: 12000,
		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", ":6379")
			if err != nil {
				panic(err.Error())
			}
			return c, err
		},
	}

	connection = connectionPool.Get()
}

// Set a key-value pair
func Set(key string, val string) error {
	_, err := connection.Do("SET", key, val)
	if err != nil {
		return err
	}
	return nil
}

// Get a value from a key
func Get(key string) (string, error) {
	val, err := redis.String(connection.Do("GET", key))
	if err != nil {
		return "", err
	}

	return val, nil
}
